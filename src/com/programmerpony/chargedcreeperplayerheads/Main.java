package com.programmerpony.chargedcreeperplayerheads;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    @Override
    public void onEnable() {
        Bukkit.getConsoleSender().sendMessage("§aChargedCreeperPlayerHeads v" + getDescription().getVersion() + " was loaded successfully!");
        getServer().getPluginManager().registerEvents(new ExplosionListener(), this);
    }

}
